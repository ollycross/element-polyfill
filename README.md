This package provides polyfills for `ParentNode`, `ChildNode` and `NodeList` methods
that are unavailable in older browsers (notably IE 11).

The polyfills are taken from the https://developer.mozilla.org/ pages. The
majority are sourced from https://github.com/jserz.

## Polyfills

The following methods are polyfilled:

-   https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/append
-   https://developer.mozilla.org/en-US/docs/Web/API/ParentNode/prepend
-   https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/remove
-   https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/before
-   https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/after
-   https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/replaceWith
-   https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach

## Installation

`npm install -S element-polyfill`

## Usage

Simply `require` the package somewhere close to the beginning of your code.

```
// require all polyfills
require('element-polyfill');

// require some polyfills
require('element-polyfill/polyfills/element.child-node.after.js')
require('element-polyfill/polyfills/element.parent-node.append.js')
```
