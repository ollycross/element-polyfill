#### 1.0.0 (2020-04-28)

##### New Features

* **polyfills:**  Added NodeList.forEach() ([840112fb](git+ssh://git@gitlab.com/ollycross/element-polyfill/commit/840112fb194e459375142deb6b442f239ad0c34e))

